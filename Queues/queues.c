#include "queues.h"

Q_queue Q_empty(const Q_size data_size)
{
     return (Q_queue) LL_empty(data_size);
}

Q_bool Q_is_empty(const Q_queue* q)
{
     return (Q_bool) LL_is_empty((LL_list*) q);
}

Q_error Q_enqueue(Q_queue* q, const void* data)
{
     return (Q_error) LL_insert_back((LL_list*) q, data);
}

void* Q_dequeue(Q_queue* q)
{
     return LL_pop_front((LL_list*) q);
}
