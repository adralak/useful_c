#ifndef QUEUES_H
#define QUEUES_H

#include "../Linked_Lists/linked_lists.h"

typedef LL_list Q_queue;
typedef LL_size Q_size;

typedef enum Q_error
{
     Q_NO_ERROR = LL_NO_ERROR,
     Q_NULL_ARG_Q = LL_NULL_ARG_LIST,
     Q_INVALID_ARG_Q = LL_INVALID_ARG_LIST,
     Q_NULL_DATA_SIZE = LL_NULL_DATA_SIZE,
     Q_ALLOC_ERROR = LL_ALLOC_ERROR
} Q_error;

typedef enum Q_bool
{
     Q_FALSE = 0,
     Q_TRUE,
     Q_NULL,
     Q_INVALID
} Q_bool;

// Create and return an empty queue
Q_queue Q_empty(const Q_size data_size);

/*
  Checks if q is empty 
  Returns:
  -Q_TRUE if the queue is empty
  -Q_FALSE if the queue is not empty
  -Q_NULL if the queue is NULL
  -Q_INVALID if the queue is invalid
*/
Q_bool Q_is_empty(const Q_queue* q);

/*
  Add data to the queue
  Returns:
  -Q_NULL_ARG_Q if q is NULL
  -Q_INVALID_ARG_Q if q is invalid
  -Q_NULL_DATA_SIZE if q->data_size is 0
  -Q_ALLOC_ERROR if an allocation error occured
  -Q_NO_ERROR on success
*/
Q_error Q_enqueue(Q_queue* q, const void* data);

/*
  Get the next element in the queue, store it in data, and remove it
  Returns:
  -NULL in case of an error
  -the data stored at the front of the queue otherwise
*/
void* Q_dequeue(Q_queue* q);

#endif
