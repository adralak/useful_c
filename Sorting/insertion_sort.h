#ifndef INSERTION_SORT_H
#define INSERTION_SORT_H

#include "utils_sort.h"

// Insertion sort arr
void SRT_insertion_sort(SRT_NUMBER_TYPE arr[], const SRT_size size);

#endif
