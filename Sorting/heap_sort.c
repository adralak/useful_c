#include "heap_sort.h"

void SRT_sift_down(SRT_NUMBER_TYPE arr[], const SRT_size size, SRT_size i);



void SRT_sift_down(SRT_NUMBER_TYPE arr[], const SRT_size size, SRT_size i)
{
     SRT_size j;
     SRT_NUMBER_TYPE curr_val = arr[i];

     while(1)
     {
	  j = 2 * i;

	  if(j <= size)
	  {
	       if(j < size)
	       {
		    if(arr[j] > arr[j + 1])
			 ++j;
	       }

	       if(curr_val > arr[j])
	       {
		    arr[i] = arr[j];
		    i = j;
		    continue;
	       }
	  }

	  arr[i] = curr_val;
	  break;
     }

     return;
}


void SRT_heap_sort(SRT_NUMBER_TYPE arr[], const SRT_size size)
{
     SRT_NUMBER_TYPE* dummy_pos = arr - 1;
     SRT_size i;
     
     for(i = size / 2; i > 0; --i)
	  SRT_sift_down(dummy_pos, size, i);

     return;
}
