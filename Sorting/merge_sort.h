#ifndef MERGE_SORT_H
#define MERGE_SORT_H

#include "utils_sort.h"
#include "insertion_sort.h"

#ifndef RUN
#define RUN 32
#endif

/*
  Iterative merge sort, not in place.

  Returns:
  -SRT_ALLOC_ERROR if malloc encountered a problem
  -SRT_NO_ERROR otherwise
*/
SRT_error SRT_merge_sort(SRT_NUMBER_TYPE arr[], const SRT_size size);

/*
  Tim sort is a merge sort with extra steps.
  It first sorts subarrays of size RUN (by default, 32, may be 
  #define'd by the user) using insertion sort, and merges the 
  subarrays from there.
  It is not in place.

  Returns:
  -SRT_ALLOC_ERROR if malloc encountered a problem
  -SRT_NO_ERROR otherwise
*/
SRT_error SRT_tim_sort(SRT_NUMBER_TYPE arr[], const SRT_size size);

#endif
