#include "insertion_sort.h"


void SRT_insertion_sort(SRT_NUMBER_TYPE arr[], const SRT_size size)
{
     SRT_size curr_sorted_size, i, where_to_insert;
     SRT_NUMBER_TYPE to_insert;

     for(curr_sorted_size = 1; curr_sorted_size < size; ++curr_sorted_size)
     {
	  to_insert = arr[curr_sorted_size];
	  // Find the index where the new element must be inserted
	  where_to_insert = SRT_bin_search(arr, curr_sorted_size,
					     to_insert);

	  // Shift all elements in [where_to_insert...curr_sorted_size]
	  for(i = curr_sorted_size; i > where_to_insert; --i)
	       arr[i] = arr[i - 1];

	  arr[i] = to_insert;
     }
}
