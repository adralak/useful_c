#ifndef UTILS_SORT_H
#define UTILS_SORT_H

#include <stdlib.h>
#include <string.h>
#include <math.h>

// SRT_NUMBER_TYPE must be a number type : int, unsigned int, double, etc
#ifndef SRT_NUMBER_TYPE
#define SRT_NUMBER_TYPE int
#endif

typedef size_t SRT_size;

typedef enum SRT_error
{
     SRT_NO_ERROR,
     SRT_ALLOC_ERROR
} SRT_error;

// Finds the maximum of arr; assumes size > 0
SRT_NUMBER_TYPE SRT_find_max(SRT_NUMBER_TYPE arr[], const SRT_size size);

// Swaps arr[i] and arr[j]; assumes i and j are indices of arr
void SRT_swap(SRT_NUMBER_TYPE arr[], const SRT_size i, const SRT_size j);

/*
  Binary search of arr; assuming arr is sorted in increasing order
  Returns the index where the element is most likely found
*/
SRT_size SRT_bin_search(SRT_NUMBER_TYPE arr[], const SRT_size size,
			const SRT_NUMBER_TYPE to_find);

// Min of two indices
SRT_size SRT_min(const SRT_size a, const SRT_size b);

// Sorts a pair, i.e. an array of two elements
void SRT_sort_pair(SRT_NUMBER_TYPE pair[]);

/*
  Unary exponential search
  Returns the index where the element is most likely found
*/
SRT_size SRT_expo_search(SRT_NUMBER_TYPE arr[], const SRT_size size,
			 const SRT_NUMBER_TYPE to_find);


#endif
