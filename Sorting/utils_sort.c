#include "utils_sort.h"


SRT_size SRT_aux_bin_search(SRT_NUMBER_TYPE arr[], SRT_size left,
			    SRT_size right, SRT_NUMBER_TYPE to_find);

SRT_NUMBER_TYPE SRT_find_max(SRT_NUMBER_TYPE arr[], const SRT_size size)
{
     SRT_size i;
     SRT_NUMBER_TYPE max;

     max = arr[0];

     for(i = 1; i < size; ++i)
     {
	  if(max < arr[i])
	       max = arr[i]; 
     }

     return max;
}


void SRT_swap(SRT_NUMBER_TYPE arr[], const SRT_size i, const SRT_size j)
{
     SRT_NUMBER_TYPE tmp = arr[i];
     arr[i] = arr[j];
     arr[j] = tmp;

     return;
}


SRT_size SRT_aux_bin_search(SRT_NUMBER_TYPE arr[], SRT_size left,
			    SRT_size right, const SRT_NUMBER_TYPE to_find)
{
     SRT_size mid;

     while(left < right)
     {
	  // Get the middle point
	  mid = (left + right) / 2;

	  // Compare and update
	  if(arr[mid] < to_find)
	       left = mid + 1;
	  /*
	    In this case, we should be able to put mid-1 in right.
	    However, since SRT_size is an unsigned type, it is
	    better not to
	  */
	  else if(arr[mid] > to_find)
	       right = mid;
	  else
	       return mid;
     }

     /*
       Some cases force us to insert after the found element.
       For example, if the searched element is greater than all the
       elements in the array.
     */
     return arr[left] < to_find? left+1:left;
}


SRT_size SRT_bin_search(SRT_NUMBER_TYPE arr[], const SRT_size size,
			const SRT_NUMBER_TYPE to_find)
{
     return SRT_aux_bin_search(arr, 0, size, to_find);
}


SRT_size SRT_min(const SRT_size a, const SRT_size b)
{
     return a<b?a:b;
}


void SRT_sort_pair(SRT_NUMBER_TYPE pair[])
{
     if(pair[0] > pair[1])
	  SRT_swap(pair, 0, 1);

     return;
}


SRT_size SRT_expo_search(SRT_NUMBER_TYPE arr[], const SRT_size size,
			 const SRT_NUMBER_TYPE to_find)
{
     SRT_size bound;

     for(bound = 1; bound < size && arr[bound] < to_find; bound *= 2);

     return SRT_aux_bin_search(arr, bound / 2, bound, to_find);
}
