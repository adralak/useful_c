#ifndef HEAP_SORT_H
#define HEAP_SORT_H

#include "utils_sort.h"

void SRT_heap_sort(SRT_NUMBER_TYPE arr[], const SRT_size size);

#endif
