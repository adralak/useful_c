#include "merge_sort.h"

SRT_error SRT_aux_merge_sort(SRT_NUMBER_TYPE arr[], const SRT_size size,
			     SRT_size curr_sorted_size);
void SRT_merge(SRT_NUMBER_TYPE arr[], SRT_NUMBER_TYPE l_arr[],
	       SRT_NUMBER_TYPE r_arr[], const SRT_size left,
	       const SRT_size mid, const SRT_size right);


void SRT_merge(SRT_NUMBER_TYPE arr[], SRT_NUMBER_TYPE l_arr[],
	       SRT_NUMBER_TYPE r_arr[], const SRT_size left,
	       const SRT_size mid, const SRT_size right)
{
     const SRT_size l_size = mid - left, r_size = right - mid;
     SRT_size i, l_i = 0, r_i = 0;

     // Copy to l_arr and r_arr
     for(i = 0; i < l_size; ++i)
	  l_arr[i] = arr[left + i];
     
     for(i = 0; i < r_size; ++i)
	  r_arr[i] = arr[mid + i];

     // Merge l_arr and r_arr into arr[left...right-1]
     for(i = left; l_i < l_size && r_i < r_size; ++i)
     {
	  if(l_arr[l_i] < r_arr[r_i])
	  {
	       arr[i] = l_arr[l_i];
	       ++l_i;
	  }
	  else
	  {
	       arr[i] = r_arr[r_i];
	       ++r_i;
	  }
     }
     
     // Add the remaining elements, if any
     for(; l_i < l_size; ++i, ++l_i)
	  arr[i] = l_arr[l_i];

     for(; r_i < r_size; ++i, ++r_i)
	  arr[i] = r_arr[r_i];
     
     return;
}

SRT_error SRT_aux_merge_sort(SRT_NUMBER_TYPE arr[], const SRT_size size,
			      SRT_size curr_sorted_size)
{
     SRT_size left, mid, right, step;
     SRT_NUMBER_TYPE *l_arr, *r_arr;
     
     // l_arr and r_arr are used in merge
     l_arr = malloc((size / 2) * sizeof(SRT_NUMBER_TYPE));
     r_arr = malloc((size / 2) * sizeof(SRT_NUMBER_TYPE));
     
     if(l_arr == NULL || r_arr == NULL)
	  return SRT_ALLOC_ERROR;

     // Start from the leaves and go up the tree
     for(; curr_sorted_size < size; curr_sorted_size *= 2)
     {
	  step = 2 * curr_sorted_size;

	  /*
	    left + curr_sorted_size >= size - 1 means we're merging
	    an array with the empty array, so that case can be ignored
	  */
	  for(left = 0; left + curr_sorted_size < size - 1; left += step)
	  {
	       mid = left + curr_sorted_size;
	       // In some cases, the right array is smaller
	       right = SRT_min(mid + curr_sorted_size, size);
	       SRT_merge(arr, l_arr, r_arr, left, mid, right);
	  }
     }

     free(l_arr);
     free(r_arr);

     return SRT_NO_ERROR;
}


SRT_error SRT_merge_sort(SRT_NUMBER_TYPE arr[], const SRT_size size)
{
     return SRT_aux_merge_sort(arr, size, 1);
}



SRT_error SRT_tim_sort(SRT_NUMBER_TYPE arr[], const SRT_size size)
{
     SRT_size i;
     
     if(size <= RUN)
     {
	  SRT_insertion_sort(arr, size);
	  return SRT_NO_ERROR;
     }

     for(i = 0; i < size - RUN; i += RUN)
	  SRT_insertion_sort(arr + i, RUN);

     SRT_insertion_sort(arr + i, size - i);
     
     return SRT_aux_merge_sort(arr, size, RUN);
}
