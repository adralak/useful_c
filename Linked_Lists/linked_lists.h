#ifndef LINKED_LISTS_H
#define LINKED_LISTS_H

#include <stdlib.h>
#include <string.h>

// These linked lists are generic
typedef struct LL_node
{
     void* data;
     struct LL_node* next;
} LL_node;

// Redef of size_t for more coherence
typedef size_t LL_size;

/* 
   Our list structure.
   It contains both the front and the back so that this data type can
   be easily translated to either a stack or a queue.
   Note that a given list uses ONE data_size; these lists are not
   meant to contain ints and longs together for example, though a union 
   can be used to solve this problem
*/
typedef struct LL_list
{
     LL_node* front;
     LL_node* back;
     LL_size data_size;
} LL_list;

// An enum for errors that may occur
typedef enum LL_error
{
     LL_NO_ERROR = 0,
     LL_NULL_DATA_SIZE,
     LL_ALLOC_ERROR,
     LL_EMPTY_LIST,
     LL_INVALID_ARG_N,
     LL_INVALID_ARG_TO_INSERT,
     LL_INVALID_ARG_N1,
     LL_INVALID_ARG_N2,
     LL_INVALID_ARG_LIST,
     LL_INVALID_ARG_LIST1,
     LL_INVALID_ARG_LIST2,
     LL_NULL_ARG_LIST,
     LL_NULL_ARG_LIST1,
     LL_NULL_ARG_LIST2,
     LL_INVALID_ARGS,
     LL_NULL_DATA
} LL_error;


typedef enum LL_bool
{
     LL_FALSE = 0,
     LL_TRUE,
     LL_NULL,
     LL_INVALID
} LL_bool;


/*
  Preface: a list is considered invalid if list->front or list->back
  is NULL and the other is not
*/


// Create and return an empty list
LL_list LL_empty(const LL_size data_size);

/* 
   Checks if list is empty. 
   Returns: 
   -LL_TRUE if the list is empty
   -LL_FALSE if the list is not empty
   -LL_NULL if the list is NULL
   -LL_INVALID if the list is invalid
*/
LL_bool LL_is_empty(const LL_list* list);

/* 
   Create a new node
   Returns:
   -The newly created node
   -NULL in case of an allocation error
*/
LL_node* LL_create_new_node(const LL_size data_size, const void* data);

/*
  Insert data at the front/back
  Returns:
  -LL_NULL_ARG_LIST if list is NULL
  -LL_INVALID_ARG_LIST if list is invalid
  -LL_NULL_DATA if data is NULL
  -LL_NULL_DATA_SIZE if list->data_size is 0
  -LL_ALLOC_ERROR if an allocation error occured
  -LL_NO_ERROR on success
*/
LL_error LL_insert_front(LL_list* list, const void* data);
LL_error LL_insert_back(LL_list* list, const void* data);

/*
  Insert to insert after n - n must not be the back node
  Returns:
  -LL_INVALID_ARG_N if n is invalid: either it is a NULL pointer or
  it is the back node
  -LL_INVALID_ARG_TO_INSERT if to_insert is a NULL pointer
  -LL_NO_ERROR on success
*/
LL_error LL_insert_after(LL_node* n, LL_node* to_insert);

/*
  Remove the front element
  Returns:
  -LL_EMPTY_LIST if called on an empty list
  -LL_NULL_ARG_LIST if list is NULL
  -LL_INVALID_ARG_LIST if list is invalid
  -LL_NO_ERROR on success
*/
LL_error LL_remove_front(LL_list* list);

/*
  Delete the node that comes after n. n must be a node of list
  Returns:
  -LL_EMPTY_LIST if called on an empty list
  -LL_NULL_ARG_LIST if list is NULL
  -LL_INVALID_ARG_LIST if list is invalid
  -LL_INVALID_ARG_N if n is either a NULL pointer or is the back node
  -LL_NO_ERROR on success
*/
LL_error LL_remove_after(LL_list* list, LL_node* n);

/*
  Get front/back values
  Returns:
  -NULL if list is a NULL pointer or if list->front/back is NULL
  -the data stored at the front/back otherwise
*/
void* LL_get_front(const LL_list* list);
void* LL_get_back(const LL_list* list);

/*
  Remove the front node, but return the data
  Returns:
  -NULL in case of an error
  -the data stored in the front node otherwise
*/
void* LL_pop_front(LL_list* list);


/*
  Swap the values of n1 and n2
  Returns:
  -LL_INVALID_ARG_N1 if n1 is NULL
  -LL_INVALID_ARG_N2 if n2 is NULL
  -LL_NO_ERROR on success
*/
LL_error LL_swap(LL_node* n1, LL_node* n2);

/*
  Reverses the list
  Returns:
  -LL_NULL_ARG_LIST if list is NULL
  -LL_INVALID_ARG_LIST if list is invalid
  -LL_NO_ERROR on success
*/
LL_error LL_reverse(LL_list* list);

/*
  Appends list2 to list1; does not allocate new memory
  Returns:
  -LL_NULL_ARG_LIST1 (resp. LL_NULL_ARG_LIST2) if list1 (resp. list2) is NULL
  -LL_INVALID_ARG_LIST1 (resp. LL_INVALID_ARG_LIST2) if list1 (resp. list2) 
  is invalid
  -LL_INVALID_ARGS if list1 and list2 have incompatible data sizes
  -LL_NO_ERROR on success
*/
LL_error LL_concat(LL_list* list1, const LL_list* list2);

/*
  Clears the contents of the list
  Returns:
  -LL_NULL_ARG_LIST if list is NULL
*/
LL_error LL_clear(LL_list* list);

// Converts an LL_error to a string
const char* LL_error_to_string(const LL_error e);

#endif
