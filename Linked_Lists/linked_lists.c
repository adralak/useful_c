#include "linked_lists.h"

void LL_remove_front_no_error(LL_list* list);

LL_list LL_empty(const LL_size data_size)
{
     LL_list empty_list;
     empty_list.front = NULL, empty_list.back = NULL;
     empty_list.data_size = data_size;

     return empty_list;
}


LL_bool LL_is_empty(const LL_list* list)
{
     if(list == NULL)
	  return LL_NULL;
     
     if(list->front == NULL)
     {
	  if(list->back == NULL)
	       return LL_TRUE;
	  else
	       return LL_INVALID;
     }

     return LL_FALSE;
}


LL_node* LL_create_new_node(const LL_size data_size, const void* data)
{
     LL_node* new_node = malloc(sizeof(LL_node));

     if(new_node == NULL)
	  return NULL;
     
     new_node->data = malloc(data_size);

     if(new_node->data == NULL)
	  return NULL;
     
     memcpy(new_node->data, data, data_size);

     new_node->next = NULL;

     return new_node;
}

LL_error LL_insert_front(LL_list* list, const void* data)
{
     LL_node* new_node;
     LL_bool empty = LL_is_empty(list);

     if(empty == LL_NULL)
	  return LL_NULL_ARG_LIST;
     else if(empty == LL_INVALID)
	  return LL_INVALID_ARG_LIST;

     if(data == NULL)
	  return LL_NULL_DATA;
     
     new_node = LL_create_new_node(list->data_size, data);

     if(new_node == NULL)
     {
	  if(list->data_size == 0)
	       return LL_NULL_DATA_SIZE;
	  else
	       return LL_ALLOC_ERROR;
     }
     
     // List is empty
     if(empty == LL_TRUE)
     {
	  list->front = new_node;
	  list->back = new_node;
     }
     // List is not empty
     else
     {
	  new_node->next = list->front;
	  list->front = new_node;
     }

     return LL_NO_ERROR;
}


LL_error LL_insert_back(LL_list* list, const void* data)
{
     LL_node* new_node;
     LL_bool empty = LL_is_empty(list);

     if(empty == LL_NULL)
	  return LL_NULL_ARG_LIST;
     else if(empty == LL_INVALID)
	  return LL_INVALID_ARG_LIST;

     if(data == NULL)
	  return LL_NULL_DATA;
     
     new_node = LL_create_new_node(list->data_size, data);

     if(new_node == NULL)
     {
	  if(list->data_size == 0)
	       return LL_NULL_DATA_SIZE;
	  else
	       return LL_ALLOC_ERROR;
     }

     // List is empty
     if(empty == LL_TRUE)
     {
	  list->front = new_node;
	  list->back = new_node;
     }
     // List is not empty
     else
     {
	  list->back->next = new_node;
	  list->back = new_node;
     }

     return LL_NO_ERROR;
}


LL_error LL_insert_after(LL_node* n, LL_node* to_insert)
{
     // Error handling
     if(n == NULL || n->next == NULL)
	  return LL_INVALID_ARG_N;
     
     if(to_insert == NULL)
	  return LL_INVALID_ARG_TO_INSERT;

     // Body
     to_insert->next = n->next;
     n->next = to_insert;

     return LL_NO_ERROR;
}


void LL_remove_front_no_error(LL_list* list)
{
     LL_node* front;
     
     front = list->front;
     list->front = front->next;
     // If the list only has one element, removing the front changes the back
     if(front->next == NULL)
	  list->back = NULL;

     free(front->data);
     free(front);
}


LL_error LL_remove_front(LL_list* list)
{
     LL_bool empty = LL_is_empty(list);

     if(empty == LL_TRUE)
	  return LL_EMPTY_LIST;
     else if(empty == LL_NULL)
	  return LL_NULL_ARG_LIST;
     else if(empty == LL_INVALID)
	  return LL_INVALID_ARG_LIST;
     
     LL_remove_front_no_error(list);
     
     return LL_NO_ERROR;
}


LL_error LL_remove_after(LL_list* list, LL_node* n)
{
     LL_node *to_remove, *next;
     LL_bool empty = LL_is_empty(list);

     if(empty == LL_TRUE)
	  return LL_EMPTY_LIST;
     else if(empty == LL_NULL)
	  return LL_NULL_ARG_LIST;
     else if(empty == LL_INVALID)
	  return LL_INVALID_ARG_LIST;
     
     // Error handling
     if(n == NULL || n->next == NULL)
	  return LL_INVALID_ARG_N;

     // Body
     to_remove = n->next;
     next = to_remove->next;

     // If the element removed is the last one, the back must be updated
     if(to_remove == list->back)
	  list->back = n;

     n->next = next;

     free(to_remove->data);
     free(to_remove);

     return LL_NO_ERROR;
}


void* LL_get_front(const LL_list* list)
{
     if(list == NULL)
	  return NULL;
     
     if(list->front == NULL)
	  return NULL;
     
     return list->front->data;
}

void* LL_get_back(const LL_list* list)
{
     if(list == NULL)
	  return NULL;
     
     if(list->back == NULL)
	  return NULL;
     
     return list->back->data;
}


void* LL_pop_front(LL_list* list)
{
     LL_bool empty = LL_is_empty(list);
     LL_node* front;
     void* data;
     
     if(empty != LL_FALSE)
	  return NULL;
          
     front = list->front;
     data = front->data;
     list->front = front->next;
     // If the list only has one element, removing the front changes the back
     if(front->next == NULL)
	  list->back = NULL;

     free(front);
     
     return data;
}


LL_error LL_swap(LL_node* n1, LL_node* n2)
{
     void* tmp = NULL;

     // Error handling
     if(n1 == NULL)
	  return LL_INVALID_ARG_N1;

     if(n2 == NULL)
	  return LL_INVALID_ARG_N2;

     // Body
     tmp = n1->data;
     n1->data = n2->data;
     n2->data = tmp;

     return LL_NO_ERROR;
}


LL_error LL_reverse(LL_list* list)
{
     LL_node *prev, *curr, *tmp;
     LL_bool empty = LL_is_empty(list);
     
     if(empty == LL_TRUE)
	  return LL_NO_ERROR;
     else if(empty == LL_NULL)
	  return LL_NULL_ARG_LIST;
     else if(empty == LL_INVALID)
	  return LL_INVALID_ARG_LIST;
     
     prev = list->front;
     curr = prev->next;
     prev->next = NULL;
     
     while(curr != NULL)
     {
	  tmp = curr->next;
	  curr->next = prev;
	  prev = curr;
	  curr = tmp;
     }

     tmp = list->front;
     list->front = list->back;
     list->back = tmp;

     return LL_NO_ERROR;
}


LL_error LL_concat(LL_list* list1, const LL_list* list2)
{
     LL_bool empty1 = LL_is_empty(list1), empty2 = LL_is_empty(list2);
     
     if(empty1 == LL_TRUE)
     {
	  *list1 = *list2;
	  return LL_NO_ERROR;
     }
     else if(empty1 == LL_NULL)
	  return LL_NULL_ARG_LIST1;
     else if(empty1 == LL_INVALID)
	  return LL_INVALID_ARG_LIST1;

     if(empty2 == LL_TRUE)
	  return LL_NO_ERROR;
     else if(empty2 == LL_NULL)
	  return LL_NULL_ARG_LIST2;
     else if(empty2 == LL_INVALID)
	  return LL_INVALID_ARG_LIST2;

     if(list1->data_size != list2->data_size)
	  return LL_INVALID_ARGS;
     
     // Body
     list1->back->next = list2->front;
     list1->back = list2->back;

     return LL_NO_ERROR;
}


LL_error LL_clear(LL_list* list)
{
     LL_bool empty = LL_is_empty(list);
     
     if(empty == LL_TRUE)
	  return LL_NO_ERROR;
     else if(empty == LL_NULL)
	  return LL_NULL_ARG_LIST;
     else if(empty == LL_INVALID)
	  return LL_INVALID_ARG_LIST;
     
     /* 
	Since the checks have already been performed,
	we don't need to peform error handling each time
     */
     while(list->front != NULL)
	  LL_remove_front_no_error(list);

     return LL_NO_ERROR;
}


const char* LL_error_to_string(const LL_error e)
{
     // static is here so that the strings are allocated
     static const char* errors[] = {"no error", "null data size", "alloc error",
				    "empty list", "invalid arg n",
				    "invalid arg to_insert", "invalid arg n1",
				    "invalid arg n2", "invalid arg list",
				    "invalid arg list1", "invalid arg list2",
				    "null arg list", "null arg list1",
				    "null arg list2", "invalid args",
				    "null data"};
     
     return errors[e];
}
