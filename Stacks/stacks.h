#ifndef STACKS_H
#define STACKS_H

#include "../Linked_Lists/linked_lists.h"

typedef LL_list S_stack;
typedef LL_size S_size;

typedef enum S_error
{
     S_NO_ERROR = LL_NO_ERROR,
     S_INVALID_ARG_STACK = LL_INVALID_ARG_LIST,
     S_NULL_ARG_STACK = LL_NULL_ARG_LIST,
     S_NULL_DATA = LL_NULL_DATA,
     S_NULL_DATA_SIZE = LL_NULL_DATA_SIZE,
     S_ALLOC_ERROR = LL_ALLOC_ERROR
} S_error;

typedef enum S_bool
{
     S_FALSE = 0,
     S_TRUE,
     S_NULL,
     S_INVALID
} S_bool;

// Create and return an empty stack
S_stack S_empty(const S_size data_size);

/*
  Checks if the stack is empty
  Returns:
  -S_TRUE if the stack is empty
  -S_FALSE if the stack is not empty
  -S_NULL if the stack is NULL
  -S_INVALID if the stack is invalid
*/
S_bool S_is_empty(const S_stack* stack);

/*
  Add an element to the stack
  Returns:
  -S_NULL_ARG_STACK if stack is NULL
  -S_INVALID_ARG_STACK if stack is invalid
  -S_NULL_DATA if data is NULL
  -S_NULL_DATA_SIZE if stack->data_size is 0
  -S_ALLOC_ERROR if an allocation error occured
  -S_NO_ERROR on success
*/
S_error S_push(S_stack* stack, const void* data);


/* 
   Removes the top elements from the stack and return it
   Returns:
   -NULL in case of an error
   -the data stored at the top otherwise
*/
void* S_pop(S_stack* stack);

#endif
