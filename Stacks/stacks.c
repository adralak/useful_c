#include "stacks.h"

S_stack S_empty(const S_size data_size)
{
     return (S_stack) LL_empty((LL_size) data_size);
}

S_bool S_is_empty(const S_stack* stack)
{
     return (S_bool) LL_is_empty((LL_list*) stack);
}

S_error S_push(S_stack* stack, const void* data)
{
     return (S_error) LL_insert_front((LL_list*) stack, data);
}

void* S_pop(S_stack* stack)
{
     return LL_pop_front((LL_list*) stack);
}
